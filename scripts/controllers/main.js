'use strict';

angular.module('examplesApp')
  .controller('MainCtrl', ['$scope', function ($scope) {
    $scope.words =
      ["Hallo", "Test", "Lorem", "Ipsum", "Lorem", "ipsum", "dolor", "sit", "amet,", "consetetur", "sadipscing", "elitr,", "sed", "diam", "nonumy", "eirmod", "tempor", "invidunt", "ut", "labore", "et", "dolore", "magna", "aliquyam", "erat,", "sed", "diam"];

    $scope.wordsObj = {
      dog: 15, cat: 4, elephant: 13, tiger: 9, hippo: 7, fish: 5, whale: 5, tomorrow: 4, today: 8, jobs: 10, giants: 3
    };

    $scope.myOnClickFunction = function (element) {
      console.log("click", element);
    };

    $scope.myOnHoverFunction = function (element) {
      console.log("hover", element);
    }
  }]);
